let express = require("express");
let path = require("path");
require("dotenv").config();

let app = express();
app.use(express.static(path.join(__dirname, "doc")));
app.set("view engine", "html");

app.get("*", (req, res) => {
  res.sendFile("/doc/index.html");
});

const port = process.env.PORT || 5000;

app.listen(port, () => `Server running on port ${port} 🔥`);
