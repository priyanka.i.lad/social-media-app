let express = require("express");
let router = express.Router();
let {
  createCommentController,
  getCommentsController
} = require("../controllers/comments");
let {
  isUserAuthorized,
  validationRules,
  validateRequest
} = require("../utils/index");
let { commentRules } = validationRules;
let isUserExists = require("../models/isUserExists");

router.post(
  "/create",
  validateRequest(commentRules),
  isUserAuthorized,
  isUserExists,
  createCommentController
);

router.get("/:post_id", isUserAuthorized, isUserExists, getCommentsController);

module.exports = router;
