var jwt = require("jsonwebtoken");
var User = require("../models/User");
var Post = require("../models/Post");
var ObjectId = require("mongodb").ObjectID;

module.exports = {
  viewProfileController: function(req, res, next) {
    let { user } = res.locals;
    let {
      name = "",
      email,
      location = "",
      phone = "",
      age = 0,
      education = "",
      gender = "",
      posts = [],
      followers = [],
      following = [],
      profilePicPath = ""
    } = user;

    return res.status(200).json({
      data: {
        name,
        email,
        location,
        phone,
        gender,
        age,
        education,
        posts,
        followers,
        following,
        profilePicPath
      }
    });
  },
  saveProfileController: function(req, res, next) {
    let { user } = res.locals;
    let { name, location, gender, age, education } = req.body;
    user.name = name;
    user.location = location;
    user.age = age;
    user.education = education;
    user.gender = gender;
    user.modifiedOn = new Date();
    const filename = res.locals.filename ? res.locals.filename : "unknown.jpeg";
    user.profilePicPath = "/profilepic/" + filename;

    User.updateOne(
      { _id: res.locals.user_id, email: req.body.email },
      user,
      err => {
        if (err) return next(err);
        return res.status(200).json({
          user: user,
          message: "User profile saved successfully"
        });
      }
    );
  },

  deleteProfileController: function(req, res, next) {
    let { user } = res.locals;
    User.updateOne(
      { _id: user._id },
      {
        $set: {
          isActive: false,
          modifiedOn: new Date()
        }
      },
      err => {
        if (err) return next(err);
        res.status(200).json({
          message: "Profile is deleted successfully"
        });
      }
    );
  },
  followUserController: function(req, res, next) {
    let { user } = res.locals;
    let { followId } = req.body;
    User.findOneAndUpdate(
      { _id: user._id },
      {
        $addToSet: {
          following: ObjectId(followId)
        }
      },
      (err, result) => {
        if (err) return next(err);
        User.updateOne(
          { _id: ObjectId(followId) },
          {
            $addToSet: {
              followers: user._id
            }
          },
          (err1, result1) => {
            if (err1) return next(err1);
            return res.status(200).json({
              followingIds: [...result.following, followId],
              message: "you are now following " + followId
            });
          }
        );
      }
    );
  },
  unFollowUserController: function(req, res, next) {
    let { user } = res.locals;
    let { unfollowId } = req.body;
    User.updateOne(
      { _id: user._id },
      {
        $pull: {
          following: ObjectId(unfollowId)
        }
      },
      (err, result) => {
        if (err) return next(err);
        User.updateOne(
          { _id: ObjectId(unfollowId) },
          {
            $pull: {
              followers: user._id
            }
          },
          (err1, result) => {
            if (err1) return next(err1);
            return res.status(200).json({
              message: "you are now unfollowing " + unfollowId
            });
          }
        );
      }
    );
  },
  getFollowersController: function(req, res, next) {
    let { user } = res.locals;
    User.findOne({ _id: user._id }, { followers: 1, _id: 0 }, (err, result) => {
      if (err) return next(err);
      return res.status(200).json({
        data: result
      });
    });
  },
  getFollowingController: function(req, res, next) {
    let { user } = res.locals;
    User.findOne({ _id: user._id }, { following: 1, _id: 0 }, (err, result) => {
      if (err) return next(err);
      User.find(
        {
          _id: {
            $in: result.following
          }
        },
        { name: 1, email: 1, profilePicPath: 1, location: 1 },
        (err1, result1) => {
          if (err1) return next(err1);
          return res.status(200).json({
            users: result1
          });
        }
      );
    });
  },
  getUsersToFollowController: function(req, res, next) {
    let user_id = res.locals.user_id;
    User.findById({ _id: user_id }, (err, user) => {
      if (err) return next(err);
      User.find(
        {
          $and: [{ _id: { $ne: user_id } }, { _id: { $nin: user.following } }]
        },
        { name: 1, email: 1, profilePicPath: 1, location: 1 },
        (err1, result) => {
          if (err1) return next(err1);
          res.status(200).json({
            users: result
          });
        }
      );
    });
  },
  getUserProfileByIdController: function(req, res, next) {
    let { userid: other_user_id } = req.params;
    User.findOne({ _id: other_user_id }, (err, user) => {
      if (err) return next(err);
      let userObj = user.toObject();
      User.find(
        {
          _id: {
            $in: user.following
          }
        },
        { name: 1, email: 1, profilePicPath: 1 },
        (err, followingUsers) => {
          userObj.following = followingUsers;
          User.find(
            {
              _id: {
                $in: user.followers
              }
            },
            { name: 1, email: 1, profilePicPath: 1, location: 1 },
            (err, followers) => {
              userObj.followers = followers;
              Post.find(
                {
                  _id: {
                    $in: user.posts
                  }
                },
                (err, posts) => {
                  userObj.posts = posts;
                  res.status(200).json({
                    user: userObj
                  });
                }
              );
            }
          );
        }
      );
    });
  }
};
