let Post = require("../models/Post");
let User = require("../models/User");
var ObjectId = require("mongodb").ObjectID;
module.exports = {
  createPostController: function(req, res, next) {
    let { user } = res.locals;
    let postObj = {
      ...req.body,
      postedBy: { id: user._id, name: user.name }
    };
    Post.create(postObj, (err, post) => {
      if (err) return next(err);
      User.updateOne(
        { _id: user._id },

        {
          $push: {
            posts: post._id
          }
        },
        (err1, user) => {
          if (err1) return next(err1);
          res.status(201).json({
            message: "Post is created"
          });
        }
      );
    });
  },
  deletePostController: function(req, res, next) {
    let { user } = res.locals;
    let { postIdToDelete } = req.body;
    Post.deleteOne({ _id: postIdToDelete }, err => {
      if (err) return next(err);
      User.updateOne(
        { _id: user._id },
        {
          $pull: {
            posts: ObjectId(postIdToDelete)
          }
        },
        (err1, result) => {
          if (err1) return next(err1);
          res.status(200).json({
            message: "Post is deleted"
          });
        }
      );
    });
  },
  getPostsController: function(req, res, next) {
    let { user } = res.locals;
    User.findOne({ _id: user._id }, { following: 1, _id: 0 }, (err, result) => {
      if (err) return next(err);
      const postedByIds = [...result.following, user._id];
      Post.find(
        {
          "postedBy.id": {
            $in: postedByIds
          }
        },
        null,
        { sort: { postedOn: -1 } },
        (err1, posts) => {
          if (err1) return next(err1);

          let updatedPosts = posts.map(post => {
            let postObj = post.toObject();
            for (let index = 0; index < postObj.likedByUsers.length; index++) {
              const userId = postObj.likedByUsers[index];
              if (userId.toString() == user._id.toString()) {
                postObj.liked = true;
                break;
              }
            }
            if (!postObj.liked) postObj.liked = false;
            return postObj;
          });
          return res.status(200).json({
            data: updatedPosts
          });
        }
      );
    });
  },
  getMyPostsController: function(req, res, next) {
    let { user } = res.locals;
    User.findOne({ _id: user._id }, { posts: 1, _id: 0 }, (err, result) => {
      if (err) return next(err);
      const postIds = result.posts;

      Post.find(
        {
          _id: {
            $in: postIds
          }
        },
        (err1, posts) => {
          if (err1) return next(err1);
          let updatedPosts = posts.map(post => {
            let postObj = post.toObject();
            postObj.likedByUsers.forEach(userId => {
              postObj.liked = userId.toString() == user._id.toString();
            });
            return postObj;
          });
          return res.status(200).json({
            data: updatedPosts
          });
        }
      );
    });
  },
  toggleLikePostController: function(req, res, next) {
    let { user } = res.locals;
    let { postId } = req.body;
    Post.findOne({ _id: postId }, (err, post) => {
      if (err) return next(err);
      if (!post) return res.status(401).json({ message: "Invalid post id" });
      if (post.likedByUsers.includes(ObjectId(user._id))) {
        Post.updateOne(
          { _id: post._id },
          {
            $pull: {
              likedByUsers: user._id
            }
          },
          (err1, result) => {
            if (err1) return next(err1);
            User.findOneAndUpdate(
              { _id: user._id },
              {
                $pull: {
                  likedPosts: post._id
                }
              },
              (err2, user) => {
                if (err2) return next(err2);
                let updatedLikedPosts = user.likedPosts.filter(
                  likedPostId => likedPostId.toString() !== post._id.toString()
                );
                return res.status("200").json({
                  likedPosts: updatedLikedPosts,
                  likeCount: post.likedByUsers.length - 1,
                  message: "Post is removed from the liked list"
                });
              }
            );
          }
        );
      } else {
        Post.updateOne(
          { _id: post._id },
          {
            $addToSet: {
              likedByUsers: user._id
            }
          },
          (err1, result) => {
            if (err1) return next(err1);
            User.findOneAndUpdate(
              { _id: user._id },
              {
                $addToSet: {
                  likedPosts: post._id
                }
              },
              (err2, user) => {
                if (err2) return next(err2);

                return res.status("200").json({
                  likedPosts: [...user.likedPosts, post._id],
                  likeCount: post.likedByUsers.length + 1,
                  message: "Post is added to the liked list"
                });
              }
            );
          }
        );
      }
    });
  }
};
