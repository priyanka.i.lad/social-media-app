let Comment = require("../models/Comment");
module.exports = {
  createCommentController: function(req, res, next) {
    let { user } = res.locals;
    let { comment, postId } = req.body;
    let commentedBy = user._id;
    Comment.create({ comment, postId, commentedBy }, (err, comment) => {
      if (err) return next(err);
      return res.status(201).json({
        message: "Comment is created"
      });
    });
  },
  getCommentsController: function(req, res, next) {
    let { post_id } = req.params;
    Comment.find({ postId: post_id }, (err, result) => {
      if (err) return next(err);
      return res.status(200).json({
        data: result
      });
    });
  }
};
