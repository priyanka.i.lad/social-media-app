var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
var { connect } = require("./db/db_connection");

let fileUpload = require("express-fileupload");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var postRouter = require("./routes/posts");
var commentRouter = require("./routes/comments");

var app = express();

connect().then(err => {
  if (err) console.log("error occured while connecting to database");
  console.log("database connected");
});
app.use(fileUpload());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "doc")));
app.use(
  "/profilepic",
  express.static(path.join(__dirname, "public/profilepics"))
);
app.use(cors());

app.use("/", indexRouter);
app.use("/user", usersRouter);
app.use("/user/post", postRouter);
app.use("/user/post/comment", commentRouter);
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.log("Error Detail: " + JSON.stringify(err));
  res.status(500).json({
    message: "Something went wrong. Please contact administrator"
  });
}
module.exports = app;
