const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: { type: String, default: "" },
  post_image_url: { type: String, default: "" },
  postedBy: {
    id: String,
    name: String
  },
  likedByUsers: { type: Array, default: [] },
  comments: { type: Array, default: [] },
  postedOn: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Post", postSchema);
