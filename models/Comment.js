const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const commentSchema = new Schema({
  commentedBy: {
    type: String,
    required: true
  },
  postId: { type: String, required: true },
  comment: { type: String, required: true },
  commentedOn: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Comment", commentSchema);
