let Validator = require("validatorjs");
module.exports = function(rules) {
  return function(req, res, next) {
    let validation = new Validator(req.body, rules, error_messages);
    if (validation.fails()) {
      let errors = validation.errors.all();
      let errors_arr = refactorErrorMessages(errors);
      return res.status(422).json({
        message: "One or more fields have incorrect data format",
        errors: errors_arr
      });
    }
    return next();
  };
};

let error_messages = {
  "required.email": "Email is required",
  "required.password": "Password is required",
  "min.password": "Password length should be 8 to 15 characters",
  "max.password": "Password length should be 8 to 15 characters",
  "regex.password": "Password must contain atleast 1 digit",
  "alpha_num.location": "Location should contain only alpha numeric values",
  "same.confirmPassword": "Password and Confirm password must be same",
  "required.confirmPassword": "Confirm password is required"
};

let refactorErrorMessages = function(errors) {
  let error_keys = Object.keys(errors);
  let errors_arr = [];
  error_keys.forEach(key => {
    for (let i = 0; i < errors[key].length; i++) {
      errors_arr.push(errors[key][i]);
    }
  });
  return errors_arr;
};

// module.exports = validateRequest;
