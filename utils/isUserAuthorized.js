var jwt = require("jsonwebtoken");
module.exports = function(req, res, next) {
  let { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).json({
      message: "User is not authorized"
    });
  }
  let token = authorization.split(" ")[1];
  jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
    if (err) {
      return res.status(401).json({
        message: "Invalid access token"
      });
    }
    let { id, email, iat } = decoded;
    if (iat > Date.now()) {
      return res.status(401).json({
        message: "Invalid access token"
      });
    }
    res.locals.user_id = id;
    return next();
  });
};
