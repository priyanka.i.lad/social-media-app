var nodemailer = require("nodemailer");
require("dotenv").config();

var transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS
  }
});

var sendMail = function(options, callback) {
  transporter.sendMail(options, (err, info) => {
    if (err) {
      return callback(err, "");
    }
    callback(null, info);
  });
};
module.exports = sendMail;
