let path = require("path");

module.exports = function(req, res, next) {
  if (req.files == null) return next();
  const file = req.files.profilePic;
  const fileExt = file.name.split(".").pop();
  const filename = res.locals.user_id + "." + fileExt;
  res.locals.filename = filename;
  const fullPath = path.join(__dirname, "../public/profilepics/", filename);
  file.mv(fullPath, err => {
    if (err) return next(err);
    return next();
  });
};
