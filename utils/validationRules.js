module.exports = {
  loginRules: {
    email: "required|email",
    password: "required|min:8|max:15|regex:/^(?=.*[0-9])/"
  },
  registerRules: {
    email: "required|email",
    name: "required|alpha",
    password: "required|min:8|max:15|regex:/^(?=.*[0-9])/",
    confirmPassword: "required|same:password"
  },
  forgotPasswordRules: {
    email: "required|email"
  },
  resetPasswordRules: {
    password: "required|min:8|max:15|regex:/^(?=.*[0-9])/",
    confirmPassword: "same:password"
  },
  userProfileRules: {
    name: "required|alpha",
    email: "required|email",
    location: "alpha_num",
    gender: "alpha"
  },
  postRules: {
    title: "required|string",
    description: "string",
    post_image_url: "string"
  },
  commentRules: {
    comment: "required|string"
  }
};
