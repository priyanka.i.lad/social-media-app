const mongoose = require("mongoose");
require("dotenv").config();

function connect() {
  return new Promise((resolve, reject) => {
    mongoose.connect(
      process.env.MONGO_URL,
      { useNewUrlParser: true, useUnifiedTopology: true },
      err => {
        if (err) {
          reject(err);
        }
        resolve();
      }
    );
  });
}

function disconnect() {
  return mongoose.disconnect();
}

module.exports = { connect, disconnect };
