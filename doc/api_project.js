define({
  "title": "API documentatation for social media app",
  "url": "https://social-media-express-api.herokuapp.com",
  "version": "1.0.0",
  "order": [
    "Registration",
    "Account Verification",
    "Login",
    "Forgot Password",
    "Reset Password",
    "Update Profile",
    "View Profile",
    "Delete Profile",
    "Follow User",
    "Unfollow User",
    "Get Followers",
    "Get Following",
    "Create Post"
  ],
  "name": "social-media-app",
  "description": "",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-02-14T07:22:56.178Z",
    "url": "http://apidocjs.com",
    "version": "0.19.1"
  }
});
