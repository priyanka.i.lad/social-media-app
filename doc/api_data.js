define({ "api": [
  {
    "type": "get",
    "url": "/verify-account/:code",
    "title": "Account Verification",
    "version": "1.0.0",
    "name": "Account_Verification",
    "group": "Index",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Account verification is successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"Account verification is successful\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/index.js",
    "groupTitle": "Index",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode-422",
            "description": "<p>Code is invalid or expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unauthorized\n{\n  \"error\": \"Code is invalid or expired\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/forgot-password",
    "title": "Forgot Password",
    "version": "1.0.0",
    "name": "Forgot_Password",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's unique Email address.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example",
        "content": "{\n  \"email\":\"abc@abc.com\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Password Reset code is sent to email address</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"Password Reset code is sent to email address\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/index.js",
    "groupTitle": "Index",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "version": "1.0.0",
    "name": "Login",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's unique Email address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"email\":\"abc@abc.com\",\n  \"password\":\"password123\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Login successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"Login successful\",\n  \"data\":{\n    \"username\":\"Abc\"\n  },\n  \"token\":<GENERATED ACCESS TOKEN>\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser",
            "description": "<p>Invalid email or password</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Invalid email or password\",\n  \"data\":{}\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/index.js",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "New Registration",
    "version": "1.0.0",
    "name": "Registration",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's unique Email address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User's display name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "confirmPassword",
            "description": "<p>User's confirm password to be compared with password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"email\":\"abc@abc.com\",\n  \"name\":\"Abc\",\n  \"password\":\"password123\",\n  \"confirmPassword\":\"password123\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Registration is successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"Registration is successful\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/index.js",
    "groupTitle": "Index",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/reset-password/:code",
    "title": "Reset Password",
    "version": "1.0.0",
    "name": "Reset_Password",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "confirmPassword",
            "description": "<p>User's confirm password to be compared with password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example",
        "content": "{\n  \"password\":\"password123\",\n  \"confirmPassword\":\"password123\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Password is reset.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"Password is reset\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/index.js",
    "groupTitle": "Index",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode-422",
            "description": "<p>Code is invalid or expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unauthorized\n{\n  \"error\": \"Code is invalid or expired\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/user/post/create",
    "title": "Create Post",
    "version": "1.0.0",
    "name": "Create_Post",
    "group": "Posts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Description of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "img_url",
            "description": "<p>Image path of the post</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"title\":\"New Post\",\n  \"desc\":\"This is new post\",\n  \"img_url\":\"images/posts/post1.jpg\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Post is created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"Post is created\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/posts.js",
    "groupTitle": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/user/post/delete",
    "title": "Delete Post",
    "version": "1.0.0",
    "name": "Delete_posts",
    "group": "Posts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "post_id",
            "description": "<p>Id of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"post_id\":Id of the post,\n  \"comment\":\"This is a new comment\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>comment is created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"comment is created\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/posts.js",
    "groupTitle": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/user/post/toggle-like",
    "title": "Toggle like the post",
    "version": "1.0.0",
    "name": "Toggle_like",
    "group": "Posts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Description of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "img_url",
            "description": "<p>Image path of the post</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"post_id\":Id of the post\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>toggle like successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"toggle like successful\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/posts.js",
    "groupTitle": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/user/delete",
    "title": "Delete Profile",
    "version": "1.0.0",
    "name": "Delete_Profile",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>User profile is deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"User profile is deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/user/follow",
    "title": "Follow User",
    "version": "1.0.0",
    "name": "Follow_User",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "follow_to",
            "description": "<p>Id of the user to follow</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"follow_to\": Id of the user to follow\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>following.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"following\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user/followers",
    "title": "List of followers",
    "version": "1.0.0",
    "name": "Get_Followers",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "followers",
            "description": "<p>list of followers' ids.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"followers\": array of followers' ids\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user/following",
    "title": "List of following",
    "version": "1.0.0",
    "name": "Get_Following",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "following",
            "description": "<p>list of user ids being followed by the user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"following\": array of following's ids\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user/unfollowing",
    "title": "List of unfollowing users",
    "version": "1.0.0",
    "name": "Get_Unfollowing",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "unfollowing",
            "description": "<p>list of user ids for user id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"following\": array of users not being followed by userid\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/user/unfollow",
    "title": "Unfollow User",
    "version": "1.0.0",
    "name": "Unfollow_User",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "unfollow_to",
            "description": "<p>Id of the user to unfollow</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"unfollow_to\": Id of the user to unfollow\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>unfollowing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"unfollowing\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/user/profile",
    "title": "Update Profile",
    "version": "1.0.0",
    "name": "Update_Profile",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User's display name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>User's Location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pic_path",
            "description": "<p>User's Image path</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>User's gender</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Body Example:",
        "content": "{\n  \"name\":\"Abc\",\n  \"location\":\"Bangalore\",\n  \"pic_path\":\"images/abc_profile_pic.jpg\",\n  \"gender\":\"male\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>User profile is updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\":\"User profile is updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidData-422",
            "description": "<p>Provided data are invalid</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"error\": \"Provided data are invalid\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user/profile",
    "title": "View Profile",
    "version": "1.0.0",
    "name": "View_Profile",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>'s profile data</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"email\":\"abc@abc.com\",\n  \"name\":\"Abc\",\n  \"location\":\"Bangalore\",\n  \"gender\":\"male\",\n  \"pic_path\" :\"images/abc_profile_pic.jpg\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/users.js",
    "groupTitle": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Bearer YOUR_ACCESS_TOKEN</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\":\"Bearer <YOUR ACCESS TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidUser-404",
            "description": "<p>User not found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAccessToken-401",
            "description": "<p>Access token is invalid or expired</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedUser-401",
            "description": "<p>User is not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 404:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"User not found\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"Access token is invalid or expired\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"User is not authorized\"\n}",
          "type": "json"
        }
      ]
    }
  }
] });
